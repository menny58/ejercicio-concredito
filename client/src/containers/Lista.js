import React, {useState, useEffect} from "react";
import Row from "../component/ListRow";


const Lista = () =>{
    const [prospect, setProspect] = useState([]);

    useEffect(() => {

        fetch("http://localhost:9000/getProspect/1",{
            method: "GET"
        })
            .then(res => res.json()).then(json => {
                json.forEach(el => {
                    let pros = {
                        id: el.IdProspecto,
                        nombre: el.Nombre,
                        apellidoP: el.PrimerApellido,
                        apellidoM: el.SegundoApellido,
                        estatus: el.Estatus,
                        comentarios: el.Comentarios
                    };
                    setProspect((prospect) =>[...prospect, pros])
                });
            });
    }, []);

    return(
        <>
            <h2>Lista de prospectos</h2>
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Primer Apellido</th>
                            <th>Segundo Apellido</th>
                            <th>Estatus</th>
                            <th>Comentarios</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            prospect.length === 0 ? <tr><td colSpan="3">Sin Prospectos</td></tr> : prospect.map(el => <Row key={el.id} el={el} />)
                        }
                    </tbody>
                </table>
            </div>
        </>
    )

}

export default Lista;