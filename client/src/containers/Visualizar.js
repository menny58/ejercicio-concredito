import React, {useEffect, useState} from "react";
import Iframe from 'react-iframe'
import Modal from "../component/Modal";
import { useModal } from "../hooks/useModal";
import "../punlic/styles/Modal.css"

const inicial = {
    nombre: "",
    apellidoP : "",
    apellidoM : "",
    calle : "",
    numero : "",
    colonia : "",
    cp : "",
    telefono : "",
    rfc: "",
    archivo: "",
    idDocumento: "",
}

const Visualizar = (props) => {
    const [isOpenModal, openModal, closeModal]= useModal(false);

    const [info, setInfo] = useState(inicial)
    const [id, setId] = useState({})
    useEffect(() => {
        setId({id: props.location.params});

        const datos = new FormData();
        datos.append("idProspecto", props.location.params);
        const url = "http://localhost:9000/getOne/"+props.location.params
        fetch(url,{
            method: "GET",
        })
        .then(res => res.json())
        .then(json => {
            let pros = json[0];
            setInfo({
                nombre: pros.Nombre,
                apellidoP : pros.PrimerApellido,
                apellidoM : pros.SegundoApellido,
                calle : pros.Calle,
                numero : pros.Numero,
                colonia : pros.Colonia,
                cp : pros.CP,
                telefono : pros.Telefono,
                rfc: pros.RFC,
                archivo: pros.DocName,
                idDocumento: pros.idDocumento
            })
        })
    },[]);

    const test = () =>{
        console.log(isOpenModal);
    }

    return(
        <>
            <h2>Información del prospecto</h2>
            <div className="container">
                    <input type="text" id="nombre" name="nombre" value={info.nombre} className="form-control" placeholder="Nombre" readOnly />
                    <input className="form-control" type="text" id="apellidoP" value={info.apellidoP} name="apellidoP" placeholder="Primer Apellido" readOnly />
                    <input className="form-control" type="text" id="apellidoM" value={info.apellidoM} name="apellidoM" placeholder="Segundo Apellido" readOnly />
                    <input className="form-control" type="text" id="calle" value={info.calle} name="calle" placeholder="Calle" readOnly />
                    <input className="form-control" type="text" id="numero"  value={info.numero} name="numero" placeholder="Numero" readOnly />
                    <input className="form-control" type="text" id="colonia" value={info.colonia} name="colonia" placeholder="Colonia" readOnly />
                    <input className="form-control" type="text" id="cp" value={info.cp} name="cp" placeholder="C.P." readOnly />
                    <input className="form-control" type="text" id="telefono" value={info.telefono} name="telefono" placeholder="Teléfono" readOnly />
                    <input className="form-control" type="text" id="rfc" value={info.rfc} name="rfc" placeholder="RFC" readOnly />
                    {info.archivo && <button onClick={openModal}>{info.archivo}</button> }
                    <Modal isOpen={isOpenModal} closeModal={closeModal}>
                        <Iframe 
                            url={"http://localhost:9000/" + info.archivo}
                            width="100%"
                            height="100%"
                            id="myId"
                            className="myClassname"
                            display="initial"
                            position="relative"
                        />
                    </Modal>
            </div>        
        </>
    )
}

export default Visualizar;