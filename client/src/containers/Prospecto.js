import React, {useState, useEffect} from "react";
import { useHistory } from "react-router";
import inicio from "./Home"

const inicial = {
    nombre: "",
    apellidoP : "",
    apellidoM : "",
    calle : "",
    numero : "",
    colonia : "",
    cp : "",
    telefono : "",
    rfc: ""
}

const Prospecto = () => {

    const [file, setFile] = useState(null);
    const [prospect, setProspect] = useState(inicial);
    const history = useHistory();

    const handelSelect = (e) => {
        setFile(e.target.files[0])
    }
    
    const handelChange = (e) => {
        setProspect({
            ...prospect,
            [e.target.name]: e.target.value
        });
    }

    const sendInfo = (e) => {
        e.preventDefault();
        if(!file){
            alert("You must select a file");
            return;
        }
        const data = new FormData();
        data.append("prospect", JSON.stringify(prospect));
        data.append("file", file);

        fetch("http://localhost:9000/insert", {
            method: 'POST',
            body: data
        })
        .then(res => {
            handleReset();
        })

        document.getElementById("fileInput").value= null
        setFile(null)
    }
    
    const handleReset = (e)=> {
        setFile({originalname: ""});
        setProspect(inicial)
    }

    const exit= () =>{
        let ex= window.confirm("Si sale de la caputra se perderan los datos capturados. ¿Seguro que desa salir?")

        if(ex){
            setProspect(inicial);
            setFile(null);
            history.push("/")
        }
    }

    return(
        <>
            <form onSubmit={sendInfo}>
                <div className="container">
                    <input type="text" id="nombre" name="nombre" value={prospect.nombre} onChange={handelChange}  className="form-control" placeholder="Nombre" required />
                    <input className="form-control" type="text" id="apellidoP" onChange={handelChange} value={prospect.apellidoP} name="apellidoP" placeholder="Primer Apellido" required />
                    <input className="form-control" type="text" id="apellidoM" onChange={handelChange} value={prospect.apellidoM} name="apellidoM" placeholder="Segundo Apellido" />
                    <input className="form-control" type="text" id="calle" onChange={handelChange} value={prospect.calle} name="calle" placeholder="Calle" required />
                    <input className="form-control" type="text" id="numero" onChange={handelChange} value={prospect.numero} name="numero" placeholder="Numero" required />
                    <input className="form-control" type="text" id="colonia" onChange={handelChange} value={prospect.colonia} name="colonia" placeholder="Colonia" required />
                    <input className="form-control" type="text" id="cp" onChange={handelChange} value={prospect.cp} name="cp" placeholder="C.P." required />
                    <input className="form-control" type="text" id="telefono" onChange={handelChange} value={prospect.telefono} name="telefono" placeholder="Teléfono" required />
                    <input className="form-control" type="text" id="rfc" onChange={handelChange} value={prospect.rfc} name="rfc" placeholder="RFC" required />
                    <input onChange={handelSelect} id="fileInput" type="file" value={setFile.originalname}  className="form-control" />
                    <div className="container">
                        <button className="btn btn-primary button button_action pull-right" type="submit" id="enviar"
                        style={{marginRight:"10px"}} data-toggle="modal" data-target="#confirmExit">Enviar</button>
                        <button className="btn btn-danger button button_action pull-right" type="button" id="salir"
                            style={{marginRight:"10px"}} onClick={exit} data-toggle="modal" data-target="#confirmExit">Salir</button>
                    </div>
                </div>
            </form>
        </>
    )
}

export default Prospecto;