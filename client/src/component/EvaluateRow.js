import React, {useState, useEffect} from "react";

const inicial = {
    coment: ""
}

const EvaluateRow = ({el, updateStatus}) =>{
    return(
        <tr>
            <td>{el.nombre}</td> 
            <td>{el.apellidoP}</td> 
            <td>{el.apellidoM}</td> 
            <td>{el.calle}</td>
            <td>{el.numero}</td>
            <td>{el.colonia}</td>
            <td>{el.cp}</td>
            <td>{el.telefono}</td>
            <td>{el.rfc}</td>
            <td><textarea name={el.id} id={el.id} cols="30" rows="3">{el.coment}</textarea></td>
            <td>
                <button className="btn btn-secondary"
                type="button" onClick={() => updateStatus(el.id, 2)}>Rechazar</button>
            </td>
            <td>
                <button className="btn btn-secondary"
                type="button" onClick={() => updateStatus(el.id, 1)}>Autorizar</button>
            </td>
        </tr>
    )
}

export default EvaluateRow;