import React from "react";
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Layout from "./component/Layout";
import Home from "./containers/Home";
import Prospecto from "./containers/Prospecto";
import Lista from "./containers/Lista";
import Visualizar from "./containers/Visualizar";
import Evaluacion from "./containers/Evaluacion";

function App() {
  return(
  <BrowserRouter>
      <Layout>
        <Switch>
          <Route exact path="/" component={Home}></Route>
          <Route exact path="/prospecto" component={Prospecto}></Route>
          <Route exact path="/lista" component={Lista}></Route>
          <Route exact path="/visualizar" component={Visualizar}></Route>
          <Route exact path="/evaluacion" component={Evaluacion}></Route>
        </Switch>
      </Layout>
  </BrowserRouter>
  )
}

export default App;
