const express = require("express");
const router = express.Router();
const multer = require("multer");
const path = require("path");
const fs = require("fs");
const DB = require("../DB/consultas")

const diskStorage = multer.diskStorage({
    destination: path.join(__dirname, "../files"),
    filename: (req, file, callback) => {
        callback(null, Date.now() + file.originalname)
    }
});

const fileIn = multer({
    storage: diskStorage
}).single("file")

router.get('/', (req, res) =>{
    // res.render('index', {title: "Concredito"});
    res.send("Welcome to server");
});

router.post("/insert", fileIn, async(req, res) => {
    // console.log(params.data.prospect.nombre);
    const pros = JSON.parse(JSON.parse(JSON.stringify(req.body)).prospect);
    const file = req.file;
    const params = {
            type : file.mimetype,
            fileName : file.originalname,
            data : fs.readFileSync(path.join(__dirname, "../files/" + file.filename)),
            userName: pros.nombre,
            apellidoP: pros.apellidoP,
            apellidoM: pros.apellidoM,
            calle: pros.calle,
            numero: pros.numero,
            colonia: pros.colonia,
            cp: pros.cp,
            telefono: pros.telefono,
            rfc: pros.rfc
        
        }
    let ok = await DB.saveFile(params);
    res.send("File saved!")
});

router.post("/updateStatus", async(req,res) =>{
    // console.log(req.body.coment);
    let params ={
        id: req.body.id,
        coment: req.body.coment,
        accion: req.body.accion
    }
    let ok = await DB.cambiarEstatus(params)
    res.send("File Updated");
})

router.get("/getProspect/:id", async(req, res) =>{
    let list = await DB.list(req.params.id);
    list.forEach(el => {
        switch (el.Estatus){
            case 1:
                el.Estatus = "Autorizado";
                break;
            case 2:
                el.Estatus = "Rechazado";
                break
            default:
                el.Estatus = "Enviado";
        }        
    });
    res.send(list)
});

router.get("/getOne/:id", async(req,res) =>{
    let id = req.params.id;
    let data = await DB.prospect(id);
    let dir = path.join(__dirname, "../dbFiles/" + data[0].DocName);
    // console.log(Buffer.from(data[0].Datos.data));
    if(data[0].Datos){
        fs.writeFileSync(dir, Buffer.from(data[0].Datos.data))
        data[0].filePath = dir;
    }
    // console.log(data[0].filePath);
    res.send(data);
    // console.log(data);
});

router.get("/getEvaluar", async(req,res) =>{
    let list = await DB.list(0);
});



module.exports = router