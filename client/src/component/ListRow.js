import React from "react";
import { Link } from 'react-router-dom'

const ListRow = ({el}) => {
    return(
        <tr>
            <td><Link to={{
                pathname: "/Visualizar",
                params: el.id
            }
            }>{el.nombre}</Link></td>
            <td>{el.apellidoP}</td>
            <td>{el.apellidoM}</td>
            <td>{el.estatus}</td>
            <td>{el.comentarios}</td>
        </tr>
    )
}

export default ListRow;