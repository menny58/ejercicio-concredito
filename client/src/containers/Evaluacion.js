import React, {useState, useEffect} from "react";
import Row from "../component/EvaluateRow";

const Evaluacion = () =>{
    const [evaluate, setEvaluate] = useState([]);
    const [toUpdate, setToUpdate] = useState(null);

    const updateStatus = (id, accion) => {
        // console.log(`id ${id} accion ${accion} textarea ${document.getElementById(id).value}`);
        const coment= document.getElementById(id).value;
        const info2={
            "a": "a"
        }
        const url = `http://localhost:9000/updateStatus/${id}/${accion}/${coment}`
        if(accion=== 2 && coment === ""){
            alert("Si se va a rechazar favor de escribir comentarios.")
        }else{
            const formdata = new FormData();
            // dat.append("json", JSON.stringify(info));
            formdata.append('id', JSON.stringify(info2));

            fetch('http://localhost:9000/updateStatus', {  
                method: 'post',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({  
                  "id" : id,
                  "accion" : accion,
                  "coment": coment
                }),
            }).then(res => {
                let asArray = Object.entries(evaluate);
                let newSend = asArray.filter(([key, value]) => value.id != id);
                llenarEvaluate(newSend);
                
                // setEvaluate(newSend)
            })
        }
    }

    const llenarEvaluate = (arreglo) =>{
        // console.log(arreglo[0][1]);
        setEvaluate([]);
        arreglo.forEach((el =>{
            // console.log(el[1].id);
            let pros = {
                id: el[1].id,
                nombre: el[1].nombre,
                apellidoP: el[1].apellidoP,
                apellidoM: el[1].apellidoM,
                calle: el[1].calle,
                numero: el[1].numero,
                colonia: el[1].colonia,
                cp: el[1].cp,
                telefono: el[1].telefono,
                rfc: el[1].rfc,
                coment: el[1].coment,
                estatus: el[1].estatus
            };
            setEvaluate((evaluate) =>[...evaluate, pros])
        }))
    }

    // const handleSubmit = (e, id) =>{
    //     e.preventDefault();


    // }

    useEffect(()=>{
        fetch("http://localhost:9000/getProspect/0",{
            method: "GET"
        })
        .then(res => res.json()).then(json =>{
            json.forEach(el => {
                let pros = {
                    id: el.IdProspecto,
                    nombre: el.Nombre,
                    apellidoP: el.PrimerApellido,
                    apellidoM: el.SegundoApellido,
                    calle: el.Calle,
                    numero: el.Numero,
                    colonia: el.Colonia,
                    cp: el.Codigo,
                    telefono: el.Telefono,
                    rfc: el.rfc,
                    coment: el.Comentarios,
                    estatus: el.Estatus
                };
                setEvaluate((evaluate) =>[...evaluate, pros])
                // setEvaluate((evaluate) =>[...evaluate, pros])
            });
        })
    },[]);

    return(
        <>
            <h2>Pendientes para Evaluar</h2>
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Primer Apellido</th>
                            <th>Segundo Apellido</th>
                            <th>Calle</th>
                            <th>Numero</th>
                            <th>Colonia</th>
                            <th>Codigo</th>
                            <th>Telefono</th>
                            <th>RFC</th>
                            <th>Comentarios</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            evaluate.length === 0 ? <tr><td colSpan="3">Sin Prospectos</td></tr> : evaluate.map(
                                el => <Row key={el.id} el={el} updateStatus={updateStatus} />)
                        }
                    </tbody>
                </table>
            </div>
        </>
        
    )
}

export default Evaluacion;