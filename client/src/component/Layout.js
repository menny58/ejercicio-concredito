import React from 'react'
import NavBar from './NavBar'
// import Footer from './Footer'

const Layout = ({ children }) => (
    <div className='App'>
        <NavBar />
        {children}
    </div>
)

export default Layout