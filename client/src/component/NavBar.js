import React from 'react'
import { Link } from 'react-router-dom'
import "../punlic/styles/Modal.css"

const NavBar = () => (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
            <a className="navbar-brand link" href="/">ConCredito</a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link to="/prospecto" className="text-light bg-dark link">Guardar Prospecto</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/lista" className="text-light bg-dark link">Lista</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/evaluacion" className="text-light bg-dark link">Evaluación</Link>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
)

export default NavBar;