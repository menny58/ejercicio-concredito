import "../punlic/styles/Modal.css"

const Modal = ({children, isOpen, closeModal}) =>{
    console.log(isOpen)
    return(
        <article className={`modall ${isOpen && "is-open"}`}>
            <div className="modall-container">
                {children}
                <button onClick={closeModal} className="modall-close btn-close-white " >X</button>
            </div>
        </article>
    )
}

export default Modal