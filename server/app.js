const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const cors = require('cors');

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.urlencoded({extended: true}));
app.use(express.json());
// app.use(require('connect').bodyParser());
app.set('port', 9000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//middlewheres
app.use(cors());

//routes
app.use(require('./routes/routes'))

//static files 
app.use(express.static(path.join(__dirname, "dbFiles")));

//listening the server
app.listen(app.get('port'), () =>{
    console.log('server on port', app.get('port'));
});